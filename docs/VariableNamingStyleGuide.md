# Variable Naming Styleguide
## For Developers

When developing on Docbuilder, maintaining a consistent style is important and helps keep code simple, concise and finally, both useable and readable.

### Variables used to process User Input

When a variable is used to process User Input, it should reflect an earlier, simpler syntax of all capitals.

```
EXPORT
```

### Variables used for core functionality

When a variable is essential to Docbuilder running, but is not User Input, it should be camel case, and be more than one word.

```
stringStripped
```

### Non-Core Variables

If a variable does not fit either of the above criteria, it should be camel case, but is not required to be multi-worded.

```
basic
```
